package com.bmeynier.article.vertx.fishs;

import com.bmeynier.article.vertx.fishs.database.FishDatabaseVerticle;
import com.bmeynier.article.vertx.fishs.database.service.FishDatabaseService;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test database interactions")
@ExtendWith(VertxExtension.class)
public class DatabaseVerticleTest {

  static FishDatabaseService dbService;

  @BeforeAll
  static void init(Vertx vertx, VertxTestContext testContext) {
    vertx.deployVerticle(new FishDatabaseVerticle(), res -> {
      dbService = FishDatabaseService.createProxy(vertx, "fishdb.queue");
      dbService.deleteAllFishs(testContext.succeeding(id -> testContext.completeNow()));
    });
  }

  @Test
  void it_should_insert_fish(VertxTestContext testContext) {
    String fish = "Discus";
    dbService.createFish(fish, res -> {
      assertThat(res.succeeded()).isTrue();
      testContext.completeNow();
    });
  }


  @Nested
  class Given_a_discus_is_already_created {

    String fish = "Discus";

    @BeforeEach
    void beforeEach(VertxTestContext testContext) {
      deleteAll().map(createFish(fish))
        .onSuccess(testContext.succeeding(id -> testContext.completeNow()))
        .onFailure(testContext.failing(id -> testContext.failNow("Impossible to create fishs")));
    }

    @Test
    void it_should_insert_delete_fish(VertxTestContext testContext) {
      dbService.deleteFish(fish, res -> {
        assertThat(res.succeeded()).isTrue();
        testContext.completeNow();
      });
    }

    @Test
    void it_should_detect_existing_fish(VertxTestContext testContext) {
      dbService.existFishByName(fish, res -> {
        if (res.succeeded()) {
          assertThat(res.result().getBoolean("exist")).isTrue();
        }
        testContext.completeNow();
      });
    }

    @Test
    void it_should_get_existing_fish(VertxTestContext testContext) {
      dbService.fetchAllFishs(res -> {
        if (res.succeeded()) {
          assertThat(res.result().encode()).contains("Discus");
        }
        testContext.completeNow();
      });
    }
  }


  @Test
  void it_should_not_existing_fish(VertxTestContext testContext) {
    String fish = "Unknown";
    dbService.existFishByName(fish, res -> {
      if (res.succeeded()) {
        assertThat(res.result().getBoolean("exist")).isFalse();
      }
      testContext.completeNow();
    });
  }


  @Nested
  class Given_two_fishs_are_already_created {

    String fish1 = "Discus";
    String fish2 = "Scalare";

    @BeforeEach
    void beforeEach(VertxTestContext testContext) {
      deleteAll().map(createFish(fish1)).map(createFish(fish2))
        .onSuccess(testContext.succeeding(id -> testContext.completeNow()))
        .onFailure(testContext.failing(id -> testContext.failNow("Impossible to create fishs")));
    }

    @Test
    void it_should_get_the_two_existing_fish(VertxTestContext testContext) {
      Checkpoint checkpoint = testContext.checkpoint(2);

      dbService.existFishByName(fish1, res -> {
        if (res.succeeded()) {
          checkpoint.flag();
        }
      });
      dbService.existFishByName(fish2, res -> {
        if (res.succeeded()) {
          checkpoint.flag();
        }
      });
    }
  }

  private Future createFish(String name) {
    Promise promise = Promise.promise();
    dbService.createFish(name, res -> {
      if (res.succeeded()) {
        promise.complete();
      }
    });
    return promise.future();
  }

  private Future deleteAll() {
    Promise promise = Promise.promise();
    dbService.deleteAllFishs(res -> {
      if (res.succeeded()) {
        promise.complete();
      }
    });
    return promise.future();
  }

}

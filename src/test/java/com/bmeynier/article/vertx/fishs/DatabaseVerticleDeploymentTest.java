package com.bmeynier.article.vertx.fishs;

import com.bmeynier.article.vertx.fishs.database.FishDatabaseVerticle;
import com.bmeynier.article.vertx.fishs.database.service.FishDatabaseService;
import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Test database deployment")
@ExtendWith(VertxExtension.class)
public class DatabaseVerticleDeploymentTest {

  Vertx vertx = Vertx.vertx();

  @Test
  void it_should_start_database_verticle() throws Throwable {
    VertxTestContext testContext = new VertxTestContext();

    vertx.deployVerticle(new FishDatabaseVerticle())
      .onSuccess(res -> testContext.completeNow())
      .onFailure(res -> System.out.println("An error occur"));

    assertThat(testContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();

    if (testContext.failed()) {
      throw testContext.causeOfFailure();
    }
  }


}
